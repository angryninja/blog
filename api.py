import os

from models import Post, Tag
from app import app, db, auth

from flask_restful import reqparse, abort, Resource, fields, marshal_with
from werkzeug import secure_filename, datastructures
from werkzeug.security import check_password_hash

@auth.verify_password
def verify_password(username, password):
    if not (username and password):
        return False
    return check_password_hash(app.config['API_USER'].get(username), password)


blog_post_fields = {
    'id': fields.Integer,
    'title': fields.String,
    'summary': fields.String,
    'body': fields.String,
    'pub_date': fields.DateTime
}

class BlogPostListAPI(Resource):

    def __init__(self):
        self.reqparse = reqparse.RequestParser()

        self.reqparse.add_argument('title', type=str, required=True,
                                   help='No Title Provided',
                                   location='json')

        self.reqparse.add_argument('summary', type=str, required=True,
                                   help='No Summary Provided',
                                   location='json')

        self.reqparse.add_argument('body', type=str, required=True,
                                   help='No Content Provided',
                                   location='json')

        self.reqparse.add_argument('tags', type=list, required=False,
                                   location='json')


    @auth.login_required
    @marshal_with(blog_post_fields)
    def get(self):
        posts = Post.query.all()
        if not posts:
            abort(404)
        return posts

    @auth.login_required
    @marshal_with(blog_post_fields)
    def post(self):
        parsed_args = self.reqparse.parse_args()
        post = Post(title=parsed_args['title'],
                    summary=parsed_args['summary'],
                    body=parsed_args['body']
                    )
        #Check if tags already exist if not add to DB
        for tag in parsed_args['tags']:
            find_tag = Tag.query.filter(Tag.tag_name == tag).first()
            if find_tag:
                p = post.post_tags.append(find_tag)
                db.session.add(post)
                db.session.commit()

            else:
                new_tag = Tag(tag_name=tag)
                p = post.post_tags.append(new_tag)
                db.session.add(new_tag)
                db.session.commit()

        return post, 201

class FileUpload(Resource):

    def __init__(self):
        self.reqparse = reqparse.RequestParser()

        self.reqparse.add_argument('file',
                                    type=datastructures.FileStorage,
                                    location='files', action='append')

    @auth.login_required
    def post(self):
        parsed_args = self.reqparse.parse_args()
        uploaded_files = parsed_args['file']

        if uploaded_files:
            for file in uploaded_files:
                filename = secure_filename(file.filename)
                file.save(os.path.join(app.config['UPLOAD_FOLDER'],filename))
            return 201

        else:
            abort(404)


#class BlogPostAPI(Resource):
#    def __init__(self):
#        self.reqparse = reqparse.RequestParser()
#
#        self.reqparse.add_argument('id', type=int, required=True,
#                                   help='No Blog ID Provided',
#                                   location='json')
#
#        self.reqparse.add_argument('title', type=str,
#                                   location='json')
#
#        self.reqparse.add_argument('summary', type=str,
#                                   location='json')
#
#        self.reqparse.add_argument('body', type=str,
#                                   location='json')
#
#        self.reqparse.add_argument('tags', type=str,
#                                   location='json')
#    @marshal_with(blog_post_fields)
#    def get(self, id):
#        post = Post.query.filter(Post.id == id).first()
#        if not post:
#            abort(404, message="Blog {0} doesn't exist".format(id))
#        return post, 201
#
#    def delete(self, id):
#        post = Post.query.filter(Post.id == id).first()
#        if not post:
#            abort(404, message="Blog {0} doesn't exist".format(id))
#        db.session.delete(post)
#        db.session.commit()
#        return 'Post {} deleted'.format(id), 201
#
#    @marshal_with(blog_post_fields)
#    def put(self, id):
#        parsed_args = self.reqparse.parse_args()
#        post = Post.query.filter(Post.id == id).first()
#        post.title = parsed_args['title']
#        post.summary = parsed_args['summary']
#        post.body = parsed_args['body']
#        post.tags = parsed_args['tags']
#        db.session.add(post)
#        db.session.commit()
#        return post, 201
