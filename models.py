from app import db
from slugify import slugify
from datetime import datetime


blog_tag = db.Table('blog_tag',
    db.Column('tag_id', db.Integer, db.ForeignKey('tag.tag_id')),
    db.Column('post_id', db.Integer, db.ForeignKey('posts.id'))
)

class Post(db.Model):
    __tablename__ = 'posts'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(80))
    slug = db.Column(db.Text, index=True)
    summary = db.Column(db.Text)
    body = db.Column(db.Text)
    pub_date = db.Column(db.DateTime)
    post_tags = db.relationship("Tag", secondary=blog_tag, backref=db.backref('posts', lazy='joined'))

    def __init__(self, title, summary, body, pub_date=None):
        self.title = title
        self.slug = slugify(title)
        self.summary = summary
        self.body = body
        if pub_date is None:
            pub_date = datetime.utcnow()
        self.pub_date = pub_date

    def __repr__(self):
        return '<Post %r>' % self.title

class Tag(db.Model):
    __tablename__ = 'tag'
    tag_id = db.Column(db.Integer, primary_key=True)
    tag_name = db.Column(db.String(20))
    tag_posts = db.relationship("Post", secondary=blog_tag, backref=db.backref('tag', lazy='joined'))

    def __init__(self, tag_name):
        self.tag_name = tag_name

    def __repr__(self):
        return '<Tag %r>' % self.tag_name
