from flask import Flask, render_template, url_for
from markdown2 import markdown

from app import app, db
from models import Post, Tag

@app.route('/', methods=['GET'])
@app.route('/index', methods=['GET'])
def index():
    threads = Post.query.order_by(Post.pub_date.desc()).paginate(per_page=7, error_out=True)

    next_url = url_for('index', page=threads.next_num) \
        if threads.has_next else None
    prev_url = url_for('index', page=threads.prev_num) \
        if threads.has_prev else None

    return render_template('index.html',
                           posts=threads.items, next_url=next_url,
                           prev_url=prev_url)

@app.route('/tags', methods=['GET'])
def tags():
    tag_list = db.session.query(Tag).all()
    return render_template('tag_index.html', tags=tag_list)


@app.route('/post/<slug>', methods=['GET'])
def post(slug):
    find_entry = db.session.query(Post).filter_by(slug = slug).first()
    if find_entry:
        render_body = markdown(find_entry.body)
        return render_template('blog_page.html', content=find_entry, body=render_body)


@app.route('/tags/<tag>', methods=['GET'])
def taged_posts(tag):
    find_entry = db.session.query(Tag).filter_by(tag_name = tag).first()
    if find_entry:
        return render_template('tag.html', posts=find_entry.tag_posts)
