import os

basedir = os.path.abspath(os.path.dirname(__file__))

class Config:
    SECRET_KEY = os.getenv('SECRET_KEY')
    DEBUG = False
    HOST = '0.0.0.0'
    PORT = '8000'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'blog.db')
    # Hash password with werkzeug.security.generate_password_hash()
    API_USER = { "admin": "####" }
    UPLOAD_FOLDER = 'static/img'


class Development(Config):
    DEBUG = True

class Production(Config):
    DEBUG = False
