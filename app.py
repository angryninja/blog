from flask import Flask
from flask_restful import Api
from flask_httpauth import HTTPBasicAuth

# Import DB
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
api = Api(app, prefix="/api/v1")
auth = HTTPBasicAuth()

app.config.from_object('config.Production')

#Instantiate DB
db = SQLAlchemy(app)


#Add the API Resources
from api import BlogPostListAPI, FileUpload
#from api_v1 import BlogPostAPI
##
api.add_resource(BlogPostListAPI, '/posts')
#api.add_resource(BlogPostAPI, '/post/<int:id>')
api.add_resource(FileUpload,'/upload')
